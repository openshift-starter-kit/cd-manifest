# 2. OpenShift GitOps を使ったアプリケーションのデプロイ

それでは、OpenShift GitOps を使って、

* 開発環境 : {GITLAB_USER}-develop
* ステージング環境 : {GITLAB_USER}-staging
* 本番環境 : {GITLAB_USER}-production

のそれぞれの環境に、アプリケーションをデプロイしていきます。

---
## 2-1. Argo CD UI へのログイン
OpenShift GitOps は内部で Argo CD というオープンソースのソフトウェアが稼働しています。  
Argo CD は GUI を持っており、OpenShift の OAuth でログインすることができます。ここでログインしてみましょう。

OpenShift Web コンソールにログインすると、画面上部に小さな四角が3x3で並んだアイコンが見つかります。  
ここをクリックすると、**"Cluster Argo CD"** というエントリーが見つかります。これをクリックします。

![Argo CD UI Login 01](../img/argocd-login-01.PNG)

すると、別ウィンドウで Argo CD の UI が開きます。

![Argo CD UI Login 02](../img/argocd-login-02.PNG)

この画面の右側にある、**"LOG IN VIA OPENSHIFT"** をクリックすると、OpenShift Web コンソールのログインの画面に移ります。  
ログインすると、次のようなアクセス認証の画面が表示されるので、**user:Info** にチェックを入れたまま **"Allow selected permissions"** ボタンをクリックします。

![Argo CD UI Login 03](../img/argocd-login-03.PNG)

Argo CD の GUI にログインします。現時点では OpenShift GitOps で何も設定していないので、"No applications yet" と何もない状態で表示されています。

![Argo CD UI Login 04](../img/argocd-login-04.PNG)

---
## 2-2. マニフェストリポジトリの接続

ログインしたら初めに、Argo CD に GitLab で Fork したあなたのマニフェストリポジトリを接続します。  
これは Argo CD がマニフェストを取得するために必要な作業です。

UI の左端にある歯車のアイコンをクリックすると、"Settings" 画面に移ります。そこで一番上にある **"Repositories"** をクリックします。

![Argo CD Repo 01](../img/argocd-repo-01.PNG)

下図のような画面になります。ここで **"CONNECT REPO USING HTTPS"** をクリックします。

![Argo CD Repo 02](../img/argocd-repo-02.PNG)

すると接続するリポジトリの情報を入力する画面が表示されます。以下の項目を入力して、**"CONNECT"** ボタンをクリックします。

* Type  
git
* Project  
(空白)
* Repository URL  
あなたの GitLab の cd-manifest リポジトリの URL ※末尾に ".git" を忘れないように注意
* Username/Password  
あなたの GitLab アカウントの username とパスワードをそれぞれ入力

![Argo CD Repo 03](../img/argocd-repo-03.PNG)

"CONNECTION STATUS" が "Successful" と表示されれば、接続に成功です。

![Argo CD Repo 04](../img/argocd-repo-04.PNG)

---
## 2-3. 開発環境でのアプリケーションのデプロイ

それでは、OpenShift GitOps を使って開発環境にアプリケーションをデプロイします。  

OpenShift GitOps には2つの重要なリソースがあります。  
1つ目は **"Application"** です。 
"Application" には、そのアプリケーションをデプロイするためのマニフェストのリンクや、デプロイ先の Project などが記録されています。  
2つ目は **"AppProject"** です。
"AppProject" は OpenShift とはまた別の "Project" の概念で、1つ以上の "Application" を稼働させるためのグループです。"Application" は何らかの "AppProject" に紐づく必要があります。

よって、これら2つのリソースを作成することが必要です。

当ハンズオンでは3つの環境ごとに、

* "{GITLAB_USER}-app-develop" Application <--> "{GITLAB_USER}-dev" AppProject 
* "{GITLAB_USER}-app-staging" Application <--> "{GITLAB_USER}-stg" AppProject 
* "{GITLAB_USER}-app-production" Application <--> "{GITLAB_USER}-prod" AppProject 

という、Application と AppProject のリソースの関係を持つことにします。

それでは開発環境の Application と AppProject を作成します。これらのマニフェストはすでに作られているので、以下のコマンドを実行するだけでよいです。

```
oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n ${GITLAB_USER}-develop

  [出力例]
  clusterrole.rbac.authorization.k8s.io/admin added: "system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller"

cd ./cd-manifest/handson/argocd

oc create -f dev-project.yaml

  [出力例]
  appproject.argoproj.io/{GITLAB_USER}-dev created

oc create -f dev-app.yaml

  [出力例]
  application.argoproj.io/{GITLAB_USER}-app-develop created
```

コマンドを実行したら、Argo CD の画面を見に行きましょう。  
先ほどは何も表示されていなかった画面に、"{GITLAB_USER}-app-develop" Application が表示されています。うまく行っていれば、Appliation の左縁は緑色になっていて、Status は "Healthy" と表示されているはずです。  
また、Project には "{GITLAB_USER}-dev" と AppProject の名前が表示されていることも確認しましょう。

![Argo CD Dev App 01](../img/argocd-dev-app-01.PNG)

Application の箱をクリックすると、この Application によってどのような OpenShift リソースがデプロイされているかなどの詳細が表示されます。

![Argo CD Dev App 02](../img/argocd-dev-app-02.PNG)

実際に OpenShift クラスタ側でもこれらのリソースを確認することができます。  
例えば、Route リソースを見に行ってみましょう。

OpenShift Web コンソールにログインし、左のメニューから **"Networking" > "Routes"** を選択し、画面上部の Project を選択する場所で **"{GITLAB_USER}-develop"** を選択します。  
すると、右側の画面で "health-record" というエントリーが見つかるでしょう。これが OpenShift GitOps によって作られた Route リソースです。  

![Argo CD Dev App 03](../img/argocd-dev-app-03.PNG)

"health-record" リソースの "Location" の列にある "http://" で始まる URL をクリックすると、別ウィンドウで "Demo Health" と表示された Web アプリケーションの画面が開きます。これが cd-app リポジトリに格納されていたアプリケーションです。

このアプリケーションはその名の通りデモアプリケーションなので、ユーザ認証の機能は実装していません。"Login" の欄で、何でもよいので適当な名前を入力して "Sign In" ボタンを押すと、次のような画面が表示されます。  

![Argo CD Dev App 04](../img/argocd-dev-app-04.PNG)

以上で、OpenShift GitOps を使って開発環境にアプリケーションをデプロイすることができました。

---
## 2-4. 同期の確認
もう少し深く Argo CD について探索していきましょう。

先ほどの Argo CD の GUI で、Application に **"Synced"** というステータス表示がありました。覚えていない方は、もう一度確認してみて下さい。 

![Argo CD Dev App 02](../img/argocd-dev-app-02.PNG)

これは、マニフェストリポジトリで宣言しているリソースと、OpenShift クラスタでデプロイされているリソースの実態が、"Sync"、つまり同期していることを意味します。

ここで、意図的にどちらかに変更を加えて、この同期をズラしてみるとどうなるでしょうか。

### 2-4-1. 実態に変更を加えた場合

OpenShift クラスタの実態の方に変更を加えてみます。例えば、現在アプリケーションの Pod は1つだけデプロイされています (上図の一番右側の列) が、これを意図的に増やしてみます。

OpenShift Web コンソールにログインし、左のメニューから **"Workloads" > "Deployments"** を選択し、画面上部の Project を選択する場所で **"{GITLAB_USER}-develop"** を選択します。  
すると、右側の画面で "health-record" というエントリーがあるので、このエントリーの右側にある、縦に点が3つ並んだアイコンをクリックします。いくつかメニューが表示されるので、一番上にある **"Edit Pod count"** を選択します。

![Argo CD Sync 01](../img/argocd-sync-01.PNG)

すると、"Edit Pod count" というポップアップが出てきます。現在は Pod が1つなので1が表示されています。これを適当な数に増やして、**"Save"** ボタンをクリックします。

![Argo CD Sync 02](../img/argocd-sync-02.PNG)

上図では、Pod count を5に設定しているので、OpenShift の機能で Pod が5つにスケールアウトされるはずです。

さて、Pod の数は増えたでしょうか ?

おそらく、増えていないでしょう。これは何度やっても増えません。反対に、Pod count を0に減らしても、すぐに1に戻ります。  
なぜ Pod の数が変わらないかと言うと、マニフェストで Pod の数を1と宣言しているためです。

OpenShift GitOps では Argo CD が常にアプリケーションを監視していて、マニフェストで宣言されている内容と実態にズレが出ると (このズレを "ドリフト" と呼びます)、マニフェスト通りになるよう自動的に修正して同期します。  
したがって、Pod の数だけではなく Route や Service など、マニフェストで宣言されているリソースは全て、ドリフトが発生すると修正されます。

この自動的に同期するポリシーを **"Auto-sync"** と呼びます。  
Auto-sync の様子は、OpenShift Web コンソールと Argo CD の画面を横に並べて、先ほどのように Pod count を増やしてみるとわかりやすいです。  
Pod count を変更すると、直ちに Argo CD の Application は **"OutOfSync"** のステータスに変化し、そして Auto-sync によって修正される様子が見られるでしょう。

![Argo CD Sync 03](../img/argocd-sync-03.PNG)

### 2-4-2. マニフェストに変更を加えた場合

今度は反対に、マニフェストに変更を加えるとどうなるか見てみましょう。先ほどと同様に、Pod の数を変化させてみます。

GitLab の cd-manifest リポジトリのソースコードの変更は、クローンしたローカルの cd-manifest リポジトリを編集して `git push` する方法でもいいですし、GitLab の WebIDE を使って直接変更する方法でも構いません。ここでは GitLab の WebIDE を使って直接変更する方法でやってみます。

まず、GitLab で Fork したあなたの cd-manifest リポジトリの画面に行きます。**master ブランチ** が選択された状態で、画面にディレクトリ/ファイル一覧が表示されています。

![GitLab CD Manifest modify 01](../img/gitlab-cd-manifest-modify-01.PNG)

当ハンズオンで Argo CD が参照しているマニフェストは、`./handson/deploy/` 以下にあります。Pod の数を宣言しているマニフェストファイルは `./handson/deploy/base/health-deploy.yaml` です。

このディレクトリ/ファイルの一覧で、**"handson" > "deploy" > "base" > "health-deploy.yaml"** と順番に辿って、"health-deploy.yaml" ファイルの画面に移ります。  

![GitLab CD Manifest modify 02](../img/gitlab-cd-manifest-modify-02.PNG)

画面には HTML のコードが表示されていますが、**"Open in Web IDE"** というボタンをクリックします。

![GitLab CD Manifest modify 03](../img/gitlab-cd-manifest-modify-03.PNG)

すると、ファイルを編集できるエディタの画面に移ります。  
`health-deploy.yaml` ファイルの 8 行目にある、`replicas: 1` という部分が Pod の数を指定している部分です。  
この部分を、`**replicas: 2**` と変更して、Pod を 2 つに増やしてみましょう。

変更したら、画面左下の **"Commit..."** ボタンをクリックします。

![GitLab CD Manifest modify 04](../img/gitlab-cd-manifest-modify-04.PNG)

上のように、変更前と変更後が左右に並んだ画面になります。変更内容に問題がなければ、左下で **"Commit to master branch"** のラジオボタンをチェックして、**"Commit"** ボタンを押します。"Commit Message" はメッセージを入力しても構いませんし、しなくても構いません。  
コミットしたら改めて "health-deploy.yaml" ファイルの中身を見て、変更が反映されていることを確認して下さい。

それでは Argo CD の UI を開いて待ちます。5分も待てば充分です。   
しばらくすると、新しい Pod が追加されて2つになります。OpenShift クラスタの方でも、Pod が2つになっていることが確認できるでしょう。

![Argo CD Sync 04](../img/argocd-sync-04.PNG)

このように、マニフェストに変更を加えると、クラスタの実態はマニフェストに合うように同期されます。

つまり、OpenShift クラスタを一切触ることなく、

**「マニフェストのみを変更することで、実態のアプリケーションに変更を加えていくことができる」** 

これこそが、GitOps の本質的な部分です。

---
## 2-5. ステージング環境でのアプリケーションのデプロイ

それでは、ステージング環境へアプリケーションをデプロイしてみましょう。

開発環境と同じように、次のコマンドを実行します。
```
oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n ${GITLAB_USER}-staging

  [出力例]
  clusterrole.rbac.authorization.k8s.io/admin added: "system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller"

cd ./cd-manifest/handson/argocd

oc create -f stg-project.yaml

  [出力例]
  appproject.argoproj.io/{GITLAB_USER}-stg created

oc create -f stg-app.yaml

  [出力例]
  application.argoproj.io/{GITLAB_USER}-app-staging created
```
開発環境では、このコマンドを実行するだけで自動的にデプロイされましたが、ステージング環境ではどうでしょうか ?  
おそらく、"{GITLAB_USER}-app-staging" Application は作られているものの、リソースはデプロイされていないでしょう。

これは、ステージング環境の "{GITLAB_USER}-app-staging" Application は、cd-manifest リポジトリの **staging ブランチ** を参照するようになっているためです。[spec.source.TargetRevision](https://github.com/argoproj/argo-cd/blob/master/manifests/crds/application-crd.yaml#L366-L370) フィールドにタグやコミット ID などを指定することができ、ここでは TargetRevision フィールドに staging タグを指定しています。

今あなたの cd-manifest リポジトリには、**master ブランチ** 1つだけある状態です。  
これは下のように、GitLab の cd-manifest リポジトリの画面で、"master" と表示されているドロップダウンメニューを開くことで確認できます。

![Argo CD Stg App 01](../img/argocd-stg-app-01.PNG)

開発環境では、Application が **master ブランチ** を参照するようになっていたので、リソースはデプロイされました。  

したがって、ステージング環境のために **staging ブランチ** を作成することが必要です。すぐに作りましょう。  
ブランチを作成する方法は GitLab の画面でも、`git` コマンドでもどちらもありますが、ここでは GitLab で作ります。  
GitLab の cd-manifest リポジトリの画面で、"cd-manifest /" 右側に "+" と表示されているドロップダウンメニューがあります。これを開いて、**"New branch"** を選択します。

![Argo CD Stg App 02](../img/argocd-stg-app-02.PNG)

"New Branch" と書かれた画面に移ります。この画面の "Branch name" に **"staging"** と入力します。

また、新しいブランチは、別のブランチを元にして、文字通り枝分かれで作成されるので、元となるブランチを指定する必要があります。  
**staging ブランチ** は **master ブランチ** を元に作成するため、"Create from" に **master** を選びます。

入力したら **"Create branch"** ボタンをクリックします。

![Argo CD Stg App 03](../img/argocd-stg-app-03.PNG)

cd-manifest リポジトリの画面に戻り、**staging ブランチ** が作られていることが確認できるでしょう。  
下図の画面で開いている、"Switch branch/tag" のメニューから **staging ブランチ** と **master ブランチ** を切り替えることができます。
切り替えてみると、両方のブランチが全く同じファイルを持っていることが確認できると思います。

![Argo CD Stg App 04](../img/argocd-stg-app-04.PNG)

さて、ここで再び Argo CD の画面で "{GITLAB_USER}-app-staging" Application を見てみましょう。どうでしょうか ?  
開発環境と全く同じように、リソースがデプロイされているはずです。デプロイされていない場合は、もう少し待ってみて下さい。

GitLab の cd-manifest リポジトリに master ブランチと全く同じファイルを持つ staging ブランチができたことで、Argo CD によって開発環境と全く同じようにステージング環境でもアプリケーションがデプロイされたことがわかります。

![Argo CD Stg App 05](../img/argocd-stg-app-05.PNG)

最後に、開発環境でも行ったように、OpenShift Web コンソールで、"{GITLAB_USER}-staging" Project に作られた "health-record" Route の Location URL にアクセスして、"Demo Health" Web アプリケーションが稼働していることを確認してみて下さい。

---
## 2-6. 【演習】本番環境でのアプリケーションのデプロイ

次は本番環境でのアプリケーションのデプロイを行います。

これは、前節のステージング環境でのアプリケーションのデプロイと、同じ手順で実施できます。  
そのため本節は演習としたいと思います。詳細な手順は記載しませんが、ヒントを残しますので、挑戦してみて下さい。

本番環境でも次のようにアプリケーションがデプロイできたら OK です。

![Argo CD Prod App 01](../img/argocd-prod-app-01.PNG)

### 【ヒント】

* 最初に実行するコマンド
```
oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n ${GITLAB_USER}-production
cd ./cd-manifest/handson/argocd
oc create -f prod-project.yaml
oc create -f prod-app.yaml
```
* GitLab でのブランチ作成  
作成するブランチの名前 : **production**  
元となるブランチ : **staging**

---
OpenShift GitOps を使ったアプリケーションのデプロイは以上です。
